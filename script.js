const video = document.getElementById('video')
const list = document.querySelector('.list')
const textBtn = document.getElementById('text');
const meshBtn = document.getElementById('mesh');
const squareBtn = document.getElementById('square');
let meshTogg = true;
let squareTogg = true;
let curr;
let textTogg = true;

Promise.all([
    faceapi.nets.tinyFaceDetector.loadFromUri('/models'),
    faceapi.nets.faceLandmark68Net.loadFromUri('/models'),
    faceapi.nets.faceRecognitionNet.loadFromUri('/models'),
    faceapi.nets.faceExpressionNet.loadFromUri('/models')
]).then(startVideo)

function startVideo() {
    navigator.getUserMedia({ video: {} },
        stream => video.srcObject = stream,
        err => console.error(err)
    )
}

video.addEventListener('play', () => {
    const canvas = faceapi.createCanvasFromMedia(video)
    document.body.append(canvas)
    const displaySize = { width: video.width, height: video.height }
    faceapi.matchDimensions(canvas, displaySize)
    setInterval(async() => {
        try{
        const detections = await faceapi.detectSingleFace(video, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceExpressions();
        const resizedDetections = faceapi.resizeResults(detections, displaySize)
        canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
        if (squareTogg) { faceapi.draw.drawDetections(canvas, resizedDetections) }
        if (meshTogg) { faceapi.draw.drawFaceLandmarks(canvas, resizedDetections) }
        if (textTogg) { faceapi.draw.drawFaceExpressions(canvas, resizedDetections) }
        // izvlacenje glavnog mood
        let express = resizedDetections.expressions
        let keys = Object.keys(express);
        let max = Math.max.apply(null, keys.map(x => express[x]))
        result = keys.reduce((_a, key) => {
            if (express[key] === max) {
                if (!(curr === key)) {
                    curr = key;
                    let time = new Date().toLocaleTimeString();
                    let date = new Date().toLocaleDateString();
                    list.innerHTML += `<li><span>${date} | ${time}</span>Current Mood: ${curr}</li>`;
                }
            }
        }, []);
    }
        catch(err){console.log(`${new Date().toLocaleTimeString()} - Face not found`)}
}, 1000)})

meshBtn.addEventListener('click', () => {
    if (meshTogg) {
        meshTogg = false;
        meshBtn.classList.remove('active')
    } else {
        meshTogg = true;
        meshBtn.classList.add('active')
    }
})

squareBtn.addEventListener('click', () => {
    if (squareTogg) {
        squareTogg = false;
        squareBtn.classList.remove('active')
    } else {
        squareTogg = true;
        squareBtn.classList.add('active')
    }
})

textBtn.addEventListener('click', () => {
    if (textTogg) {
        textTogg = false;
        textBtn.classList.remove('active')
    } else {
        textTogg = true
        textBtn.classList.add('active')
    }
})